# Pressures
"""
Superimposed pressure : pressure due to the weight of the lung above + body
Concept introduced in Hickling (1998)
"""

function SI_pressure(csv_file)

    data_csv = CSV.File(csv_file, header = 0; types = Float64)

    if size(data_csv)[1] != 2^N
        throw(DomainError(size(data_csv)[1], "Superimposed pressure must be of size 2^N"))
    end
    Psi = zeros(2^N, 1)
    Psi = data_csv.Column1
    Psi = Psi * 0.010197#converion to cmH2O

    return Psi

end

"""
Computation of the time evolution of the pleural pressure for each compartment
x_eso : esophagus x coordinate (= middle)
coord : vector containing the coordinates (x,y,z) of a compartment
"""

function pleural_pressure(coord, x_eso, pos)

    grad = pleural_grad(pos)
    if pos=="sup"
        Pes = 5
    else
        Pes = 7
    end
    delta_x = coord[1] - x_eso
    Ppl = Pes+ delta_x * grad

    return Ppl
end

"""
Computation of the pleural pressure gradient with respect to the position
unit : cmH2O/cm
"""
function pleural_grad(pos)

    if pos == "pro"
        grad = -0.29
    elseif pos == "sup"
        grad = 0.55
    else
        throw(DomainError(pos, "position must be either 'sup' (supine) or 'pro' (prone)"))
    end
end


"""
Computation of the time evolution of the tracheal pressure
t : time
RR : respiratory rate (.min-1)
tau : inspiratory to expiratory ratio
Peep : positive end expiratory pressure
Ppeak : max pressure
"""
function tracheal_pressure(t)

    T = 60 / RR
    t_within_T = t - floor(t / T) * T
    Tinsp = tau * T / (1 + tau)
    if (t_within_T > Tinsp)
        Ptr = Peep
    else
        Ptr = Pplat
    end

    return Ptr

end
