"""
Functions dedicated to the simulation of recruitment maneuvers
"""

function ReversePosition(pos, SIP, Ppl, ball_coords, x_eso) 

    if  pos== "sup"
        pos = "pro"
        for j = 1:2^N
            Ppl[j] = pleural_pressure(ball_coords[j, :], x_eso, pos)
            SIP[j] = 0
        end
    elseif pos == "pro"
        pos = "sup"
        file = path * "SIP_" * pos * "_" * patient_ID * "_" * string(N2) * ".csv"
        SIP = SI_pressure(file) 
        for j = 1:2^N
            Ppl[j] = pleural_pressure(ball_coords[j, :], x_eso, pos)
        end
    else
        throw(DomainError(pos, "wrong entry for position"))
    end
    return pos, SIP, Ppl
end

function PeepTrial(Peep, Pplat, incr)
    Peep += incr
    Pplat += incr
    if Pplat > 30
        Pplat = 30
    end
    return Peep, Pplat
end
