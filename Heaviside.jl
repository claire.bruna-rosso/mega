# Definition of the Heaviside function

function Heaviside(x::Vector{T}) where {T<:Number}
    H = similar(x,T)
    for i in eachindex(x)
        H[i] = Heaviside(x[i])
    end
    return H
end

function Heaviside(x::Array{T}) where {T<:Number}
   H = similar(x,T)
   for i in eachindex(x)
      H[i] = Heaviside(x[i])
   end
   return H
end

function Heaviside(x::Number)
    if x>0
        return 1
    else
        return 0
    end
end