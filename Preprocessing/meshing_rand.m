clear variables
close all
clc

%List of patients
patients = {'Patient1' 'Patient2' 'Patient3' 'Patient4'};

for kk = 1:length(patients)

    patient = patients{1,kk};
    load(['\\smspfim\datalba$\home\bruna-rosso\Projets\MEGA\Donnees_cliniques\' patient '_CTData.mat']);
    l=0;
    % Vector that contains all the indice triplets of the voxels that
    % belong to the lung
    indices = zeros(size(density,1)*size(density,2)*size(density,3),3); 
    % 3D array of triplets (that makes it a 4D array) that has the same
    % size as the CT scan. Each voxels belonging to the lung is given a
    % triplet (i,j,k) corresponding to its position within the CT. The
    % voxels outside the lung have a triplet = [0,0,0]
    indices2 = zeros(size(density,1),size(density,2),size(density,3),3); % triplet stored in 3D array form
    for i=1:size(density,1)
        for j=1:size(density,2)
            for k=1:size(density,3)
                if(density(i,j,k)>0) % selection of the voxels that belong to the lungs 
                    l=l+1;
                    indices(l,:)=[i j k];
                    indices2(i,j,k,:)=[i j k];
                end
            end
        end
    end
    indices(l+1:end,:)=[];
 %%  
    for ii = 2:5 % each number will set how much point is taken in each direction 

        Np = 2^ii; % number of points on each axis
        
        %%
        %selection of Np^3 voxels belonging to the lung in a random
        %manner
        picked_idx = indices(ceil(rand(Np^3,1)*l),:);
        stepx = spacing(1);
        stepy = spacing(2);
        stepz = spacing(3);

        % Coordinates of the selected voxels that will constitute the
        % center of each compartment.
        coords = [picked_idx(:,1)*stepx, picked_idx(:,2)*stepy, picked_idx(:,3)*stepz];
        coordsx = indices2(:,:,:,1)*stepx;
        coordsy = indices2(:,:,:,2)*stepy;
        coordsz = indices2(:,:,:,3)*stepz;
        
        % Radius (in voxel) of the volume taken around each voxel picked 
        % All the variables computed for each compartment (density, SIP
        % ...) will be averaged over this volume

        xmax1 =  max(indices(:,1));
        xmin1 =  min(indices(:,1));
        r1 = floor((xmax1-xmin1)/Np/2);   
        
        ymax2 =  max(indices(:,2));
        ymin2 =  min(indices(:,2));
        r2 = floor((ymax2-ymin2)/Np/2);   
          
        zmax3 =  max(indices(:,3));
        zmin3 =  min(indices(:,3));
        r3 = floor((zmax3-zmin3)/Np/2); 
        
        % Initialization of the variables averaged over a volume of the
        % lung

        dens = zeros(Np^3,1); % Density
        HU = zeros(Np^3,1); % Hounsfield units
        SIP_av = zeros(Np^3,1); % Averaged super imposed pressure
        coeff = zeros(Np^3,1); % Coefficient to ponder mechanical and physiological properties
        voxels = zeros(Np^3,1); % Number of voxels inside each volume
        tissue_type = zeros(Np^3,3); % 1: "ground glass" 2: closed units (atelectasis) 3: healthy tissue 
        % 
        for i = 1:Np^3
            voxel = 0; % Number of voxels inside the current volume
            for j = -r1:r1
                for k = -r2:r2
                    for m = -r3:r3
                        if picked_idx(i,1)+j <= size(density,1) && picked_idx(i,2)+k <= size(density,2) && picked_idx(i,3)+m <= size(density ,3)...
                           && picked_idx(i,1)+j > 0 && picked_idx(i,2)+k > 0 && picked_idx(i,3)+m > 0 %% To remain within the CT-scan boundaries
                            if density(picked_idx(i,1)+j, picked_idx(i,2)+k, picked_idx(i,3)+m) > 0 %% Test to see if the visited voxel is in the lung
                                voxel = voxel+1; % incrementation of the number of voxels within this volume 
                                dens(i) = dens(i) + density(picked_idx(i,1)+j, picked_idx(i,2)+k, picked_idx(i,3)+m); % Addition of all the densities belonging to the current compartment volume
                                HU(i) = HU(i) + double(DicomVol(picked_idx(i,1)+j, picked_idx(i,2)+k, picked_idx(i,3)+m)); % Addition of all the HUs belonging to the current compartment volume
                                SIP_av(i) = SIP_av(i) + SIP(picked_idx(i,1)+j, picked_idx(i,2)+k, picked_idx(i,3)+m);  % Addition of all the SIPs belonging to the current compartment volume
                                if Segment(picked_idx(i,1)+j, picked_idx(i,2)+k, picked_idx(i,3)+m)==1
                                    tissue_type(i,1) = tissue_type(i,1)+1;
                                elseif Segment(picked_idx(i,1)+j, picked_idx(i,2)+k, picked_idx(i,3)+m)==2
                                    tissue_type(i,2) = tissue_type(i,2)+1;
                                elseif Segment(picked_idx(i,1)+j, picked_idx(i,2)+k, picked_idx(i,3)+m)==3
                                    tissue_type(i,3) = tissue_type(i,3)+1;
                                end
                            end
                        end
                    end
                end
            end
            dens(i) = dens(i)/voxel; % averaging
            HU(i) = HU(i)/voxel; % averaging
            SIP_av(i) = SIP_av(i)/voxel; % averaging
            voxels(i) = voxel; % Storage of the number of voxels in each sub-volume
        end

        % Affectation of tissue type : we select the type that has the most
        % pixels within each compartment

        Type = zeros(Np^3,1);

        for i=1:Np^3
            [maxi,index] = max(tissue_type(i,:));
            Type(i) = index;
        end

        % Computation of the coefficients that scale mechanical and 
        % physiological properties w.r.t the volume averaged HU
        % The HU thresholds to separate the different lung areas were 
        % retrieved from Gattinoni et al. 2006.
        
        for i = 1:length(HU)
            if Type(i)==1 || Type(i)==3
               if HU(i)> -100 % non inflated lung
                   coeff(i)=0.01;
               elseif HU(i) > -500 % poorly inflated lung
                   coeff(i) = -2e-3*HU(i)-2.5e-3;
               else %normally or overinflated lung
                   coeff(i) = 1;
               end
            else
                coeff(i)=1;
            end
        end
       
       
 %%
        writematrix(coords,['./Model_inputs/coords_' patient '_' int2str(ii) '.csv'])
        writematrix(HU,['./Model_inputs/HU_' patient '_' int2str(ii) '.csv'])
        writematrix(dens,['./Model_inputs/density_' patient '_' int2str(ii) '.csv'])
        writematrix(SIP_av,['./Model_inputs/SIP_sup_' patient '_' int2str(ii) '.csv'])
        writematrix(coeff,['./Model_inputs/coeff_' patient '_' int2str(ii) '.csv'])
    end
end