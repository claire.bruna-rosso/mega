clear variables
close all
clc
% This script is intended to compute density and superimposed pressure
% distribution within the lung from 1. Patient CT-scans 2. Lung masks
% 

% IDs of the patients
patient_IDs = {'Patient1' 'Patient2' 'Patient3' 'Patient4'};

% Directory where to find the CT scans
datadir = '\\smspfim\datalba$\home\bruna-rosso\Projets\MEGA\Donnees_cliniques';

%%
for ii = 1:length(patient_IDs)
   
    patient  = patient_IDs{ii};
   
    %CT-scan reading and useful data retrieval

    if isfile([datadir '/' patient '_CTData.mat'])
        load([datadir '/' patient '_CTData.mat'])
    else
        DicomVol = int16(squeeze(dicomreadVolume([datadir '\CT-Scan\' patient])));
        DCMfiles = dir([datadir '\CT-Scan\' patient '\*.DCM']);
        DicomInfo = dicominfo([datadir '\CT-Scan\' patient '\' DCMfiles(1).name]) ;
        DicomVol = DicomVol*DicomInfo.RescaleSlope+DicomInfo.RescaleIntercept;
        spacing = zeros(3,1);
        spacing(3) = DicomInfo.SpacingBetweenSlices;
        spacing(1:2) = DicomInfo.PixelSpacing;
    end

    % Segmentation (mask) import 
    Segment = nrrdread([datadir '\Segmentations\' patient '.nrrd']);

    % CT-scan and Segmentation alignment (may be useless depending on your
    % own files)
    Segment = permute(Segment,[2 1 3]);
    Segment = flip(Segment,1);
%%  
    % Output of the scan and segmentation to verify that they are aligned
    figure
    imagesc(DicomVol(:,:,200))
    colormap(gray)
    colorbar

    figure
    imagesc(Segment(:,:,200))
    colorbar
    

    %%
    mu_air=1.22;
    mu_water = 1000;

    % Computation of the density distribution from Housfield Units
    density = DicomVol*(mu_water-mu_air)/1000+mu_water;

    voxel_vol = spacing(1)*spacing(2)*spacing(3);
    mass = 1e-9*voxel_vol*double(density);

    mass(Segment==0)=0;
    density(Segment==0)=0;
    g=9.81;
    
%%

    % Computation of the superimposed pressure distribution

    SIP = zeros(size(mass,1),size(mass,2),size(mass,3));
  
    for j=1:size(mass,2)
        for k=1:size(mass,3)
            for i=1:size(mass,1)
                if(mass(i,j,k)<0)
                    mass(i,j,k)=0;
                end 
                SIP(i,j,k) = sum(mass(1:i,j,k))*g/(spacing(2)*spacing(3)*1e-6);
            end
        end
    end
%%
    % Visualization of the SIP in one scan
    figure
    imagesc(SIP(:,:,200))
    colorbar

    %title(patient)
%%
    save(['./' patient '_CTData.mat'],'DicomVol','Segment','SIP','density','spacing')   
end

