#=
file containing all the parameters of the model
=#

# Input pressures (tracheal and pleural) related parameters
 tau = 1 / 2 # expiration / inspiration time ratio
 RR = 20 # Respiratory rate (min-1)
 Peep =  16# Positive end expiatory pressure
 Pplat = 26# Mechanical ventilation max. pressure
 Pes = 2 # Esophagal pressure during inspiration
 Qin = 0.6 # Air flow delivery in VC ventilation
 V_FRC = 1.2 # Functional residual capacity volume
 prone_positioning = false # True if the patient is flipped during the simulation
 Peep_tr = false # True if Peep trial during the simulation

 N = 9# Number of generations
if N > 15
    throw(DomainError(N, "The tree model is not valid for generation number > 15"))
end

V_repart = ones(2^N) / 2^N * 100  # percentage of lung volume distribution over all balloons
if sum(V_repart) != 100
    throw(DomainError(V_repart, "Initial volume repartitions have to add to 100%"))
elseif length(V_repart) != 2^N
    throw(DimensionMismatch("V_repart must be of length 2^N"))
else
    V_repart = V_repart * 1e-2
end

coeff_Vinit = 1.2
V_init = coeff_Vinit*V_FRC * V_repart # Initial lung volume distribution

# Recruitment variables (Massa et al. 2008)
sigma_p = 3
mu_p = 5
So = 0.04
Sc = 0.004
DeltaP = 4 # Difference between opening and closing pressures 

# Acini mechanical model parameters (Swan 2012)
ksi = 2500 #Pa
a = 0.433 #no unit
b = -0.661 #no unit

# Other hyperelastic model parameters (Birzle 2019)

c3 = 20#Pa 
d3 = 5
nu = 0.3
E = 1913.7*1.5 #Pa
coeff_V0 = 0.4
beta = nu / (1 - 2 * nu)
c = E / (4 * (1 + nu)) #Pa

# Physiological model parameters 

n = 2.5        # Parameter of the Hill's saturation curve
P_tilde = 26   # Parameter of the Hill's saturation curve

c0 = 0.2       # O2 fraction in breathed air
Pv =  40       # O2 partial pressure in venous blood (default 30)
Dm = 3.5e-4/2^N   # Membrane diffusing capacity of O2
sigma = 1.4e-6 # O2 solubility in plasma
Vc = 70e-3/2^N     # Capillary volume
C = 2.2e-3     # Concentration of hemoglobin
heart_rate = 100 # heartbeat rate min-1
tau_b = 60/heart_rate   # Arterial time period = heart beat period 

# Parameters introduced for the Physio-Mechanical Coupling,(Martin&Maury,2013), (Jbaily, 2020)

MO = 0.032     # Molar mass of Oxygen kg/mol
MG = 0.029     # Molar mass of the gas mixture (air) kg/mol
Patm = 760     # Atmospheric Pressure mmHg
P_vapor = 47    # Vapor pressure in human lungs mmHg

# Patient

patient_ID = "Patient4"
pos = "sup" # Patient position (prone or supine)

#Numerical parameters
max_iter = 100 # max number of iteration in newton algorithm
alpha = 1 # increment size ponderation
tol = 1e-5 # residual and increment tolerance for Newton iterations convergence

# time discretization related parameters
Ncycle = 3 # Number of respiratory cycles to be performed
endtime = Ncycle*60 / RR # End time of the simulation
flip_time = endtime/2 # time for flipping the position from prone to supine or the other way around. 0 if no flipping
time_Peep_trial = [endtime*0.25 endtime*0.40 endtime*0.55 endtime*0.70 endtime*0.85] # time to perform Peep increment if Peep_trial = true
incr_Peep_trial = 2 # increment of Peep at each trial step
delta_t = 0.02 # Time step
theta = 1 # Time integration parameter

# Output directory definition
output_dir = "C:/Users/bruna-rosso/Documents/Recherche/MEGA/Resultats_simu/" * patient_ID *'_'* string(Dates.today())