#============================================================#
# Physio - Mechanical coupling 
#============================================================#

#============================================================#
# OUTPUTS OF THE MECHANICAL MODEL (INPUT OF THE PHYSIOLOGICAL MODEL)
#============================================================#
# P_A(t) ----> Alveolar Pressure 
# V(t)   ----> Alveolar Volume 
# x(t) ----> Recruitment variable 
# state(t) ----> Opening state of the alveoli 
#============================================================#

#============================================================#
# OUTPUTS OF THE PHYSIOLOGICAL MODEL
#============================================================#
# P(t) ----> Oxygen Partial Pressure in the Capillary System
# c_A(t) ----> Oxygen Concentration in the alveoli
#============================================================#

# Libraries
using ForwardDiff    # for automatic differentiation
using LinearAlgebra  
using DelimitedFiles # for csv export 
using Distributions # for statistical distributions 
using Statistics
using CSV
using Dates

# Definition of the directory where to find the inputs files 
isdef = @isdefined localPath
if isdef == true
    path = localPath
elseif ARGS != String[]
    path = ARGS
else
    print("Enter the path to the input file directory \n") 
    path = readline()
end

# File containing all the parameters of a simulation
include("Parameters.jl")

print("Beginning of the simulation \n")
print(Dates.format(now(), "HH:MM") * "\n")

N2 = Int(N / 3)

# Time vector
t = zeros(floor(Int, endtime / delta_t))

for i = 2:lastindex(t)
    t[i] = t[i-1] + delta_t
end

#
file2 = path * "coeff_" * patient_ID * "_" * string(N2) * ".csv"

data_csv = CSV.File(file2, header = 0; types = Float64)

if size(data_csv)[1] != 2^N
    throw(DomainError(size(data_csv)[1], "Coefficient must be of size 2^N"))
end
global coeff = zeros(2^N, 1)
coeff = data_csv.Column1

# Creation of the output directory if not already existing
if(!isdir(output_dir))
    mkdir(output_dir)
end

#==============================MECHANICAL MODEL====================================#
# Mechanical Variables, P_A(t) and V(t), for each respiratory unit (RU) j (j in {1,2,...,2^N})
include("PressureModel.jl")

#Mechanical variable initializations

V = zeros(2^N, length(t))
P_A = zeros(2^N, length(t))
Comp = zeros(2^N, length(t))
state = Array{Bool,2}(undef, 2^N, length(t))
Ppl = zeros(2^N) 
x = ones(2^N, length(t)) 


MechanicalModel(V, P_A, x, state, Comp, Ppl)
  
#================================MECHANICAL OUTPUTS=========================================#
writedlm(output_dir * "/Compliances_" * pos * "_N" * string(N) * ".csv", Comp, ',') # Compliance
writedlm(output_dir * "/Volumes_" * pos * "_N" * string(N) * ".csv", V, ',') # Compartment volumes
writedlm(output_dir * "/Pressures_" * pos * "_N" * string(N) * ".csv", P_A, ',') # Compartment absolute pressures
writedlm(output_dir * "/Ppl_" * pos * "_N" * string(N) * ".csv", Ppl, ',') # Pleural pressures
writedlm(output_dir * "/Recruitment_" * pos * "_N" * string(N) * ".csv", x, ',') # Recruitment variable
writedlm(output_dir * "/Atelect_" * pos * "_N" * string(N) * ".csv", Int.(state), ',') # Opening RU states
writedlm(output_dir * "/coeff_" * pos * "_N" * string(N) * ".csv", coeff, ',') # inflammation variable
#=============================================================================================#

#= Compute the Oxygen Partial Pressure in capillaries P_cap(t),the Oxygen concentration c(t), 
   (Martin & Maury, 2013)
=#
include("Physiological_Model.jl")

# Initialization of the O2 Partial Pressure vector P_cap(t) in the capillaries
P_cap = zeros(2^N, length(t))
# Initialization of the oxygen concentration vector c(t)
c_A = zeros(2^N, length(t))
# Initialization of the oxygen alveolar partial pressure PO2_alv
PO2_alv = zeros(2^N, length(t))

PhysiologicalModel(P_cap, c_A, PO2_alv)

#================================PHYSIOLOGICAL OUTPUTS=========================================#
writedlm(output_dir * "/Partial_P_Cap_" * pos * "_N" * string(N) * ".csv", P_cap, ',') # 02 partial pressure in the capillaries 
writedlm(output_dir * "/O2_aw_conc_" * pos * "_N" * string(N) * ".csv", c_A, ',') # O2 concentration in the alveoli
writedlm(output_dir * "/PO2alv_" * pos * "_N" * string(N) * ".csv", PO2_alv, ',') # O2 concentration in the alveoli

println("*******SIMULATION FINISHED********")
print(Dates.format(now(), "HH:MM") * "\n")
