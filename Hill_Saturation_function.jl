# Declarartion of the Hill's function (Saturation of the oxygen) and its derivative (Martin&Maury, 2013);

# Hill's curve
function Saturation(P_O2) 
    Sat = 0

    if P_O2 > 0
        Sat = P_O2^n/(P_O2^n + P_tilde^n)
    else
        Sat = 0
    end

    return Sat

end
# Derivative of the Hill's curve
Saturation_prime(P_O2) = ForwardDiff.derivative(P_O2->Saturation(P_O2), P_O2)