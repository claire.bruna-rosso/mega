# README #

This software is licensed under the EUPL. A copy is provided is the file *License.md*

### What is this repository for? ###

This repository includes a simplified lung model developed to study the ARDS (acute respiratory distress syndrome) lung biomechanics and physiology.
A paper explaining the theory and part of the implementation in preparation... hope to be able to deliver it soon!

### How do I get set up and use this code? ###

* This model is implemented using the Julia language, so it has to be installed before being able to run it
* The code requires the installation of the following packages *Statistics*, *CSV*, *LinearAlgebra*, *Distributions*, *ForwardDiff*, *DelimitedFiles*, *Dates*,
In any case, if you miss one package Julia will tell you to install it!
* The input files have to be regrouped in one directory that is given in the variable "localpath". If you forget to define it before lauching the simulation, don't worry, the code will ask for it.
* The output files are generated in CSV format in a directory store in the variable "output_dir". I give it a default value in the code, set it as it suits you best!
* I do my best to make the code structured and well-commented, but do not hesitate to contact me (see below) to have extra explanations or point out something I should detail or refactor. 

### Contact ###

* Author : *Claire* claire.bruna-rosso@univ-eiffel.fr  
All comments, suggestions, bug report, feedback, etc. are very welcomed!
