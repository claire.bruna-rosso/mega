#=
Recruitment/Derecruitment variable computation
Model retrieved from Bates & Irving (2002)
=#

# Uniform distribution of the opening and closing speed (Massa et al. 2008)
ksi1 = rand(Uniform(0,1),1,2^N)
ksi2 = rand(Uniform(0,1),1,2^N)

sc = Sc./ksi2
#so = So./ksi1
so = 10*sc

# Normal distribution of the closing pressure (Massa et al. 2008)
Pcrit = rand(Normal(mu_p,sigma_p),2^N)

"""
Increment of recruitment/derecruitment variable computation
"""

function d_x(P,i)
    
    Po = Pcrit[i] + DeltaP
    dx = 0.0
    if P <= Pcrit[i] # closure
        dx = (P-Pcrit[i])*sc[i]
    elseif P >= Po #opening
        dx = (P-Po)*so[i]
    else
        dx = 0
    end
    return dx
end

"""
State of recruitment of the balloons
"""
function is_open(state,x)

    state_new = state
    if state==true && x>=1.0 # open & x=1
        state_new = true
    elseif state==false && x>=1.0 # closed and x=1
        state_new = true
    elseif state==true && x<=0.0 # open and x=0
        state_new = false
    elseif state==false && x<= 0.0 # closed and x=0
        state_new = false
    end
    return state_new
end
