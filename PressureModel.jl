

#=
Simplified lung model based on the "exit compartment model" of
Pozin et al., Int J Numer Meth Biomech Engng. 2017; 33:e2873
Pressure driven model (unknown volume)
=#    
include("Geometry.jl")
include("Pressures.jl")
include("Compliances.jl")
include("Recruit.jl")
include("R_tree.jl")
include("NewtonIter_Mech.jl")
include("RecruitManeuvers.jl")

function MechanicalModel(V, P_A, x, state, Comp, Ppl)

    # Compartment coordinates from input file generated from patient CT scans
    ball_coords = ballcoords( path * "coords_" * patient_ID * "_" * string(N2) * ".csv")
    ball_coords = ball_coords * 0.1 #conversion in cm
    x_eso = Statistics.mean(ball_coords[1, :])
    
    # tracheal pressure
    Ptrach = zeros(2^N, length(t)) 
    
    # pleural pressure
    
    for j = 1:2^N
        Ppl[j] = pleural_pressure(ball_coords[j, :], x_eso, pos)
    end

    # Vector containing each compartment volume at each time step
    V[:, 1] = V_init
    V_tilde = zeros(2^N, length(t)) # V-V[0]


    # Opening state of each compartment
    state[:,1] .= true; # All respiratory units initially opened 
    
    # Variable tracking if a patient has already been flipped from supine to prone
    # position (or the other way around)
    flipped = false

    # Variable storing the current Peep_trial step 
    Peep_step = 0

    # super-imposed pressure
    SIP = zeros(2^N) 
    if(pos == "sup")
        file = path * "SIP_" * pos * "_" * patient_ID * "_" * string(N2) * ".csv"
        SIP = SI_pressure(file)
    end
   
    # Flow resistance matrix of the tracheobronchial tree 
    Mat_R = R_tree(R, L)

    # Matrix of compartment compliances
    for j = 1:2^N
        Comp[j, 1] = compliance(V_init[1], j)
    end

    # Pressure in each compartment 
    P_A[:, 1] = Peep * ones(2^N) - Ppl - SIP

    # Right-hand side vector 
    rhs = zeros(2^N, length(t))
    
    # Tracheal pressure (imposed by mechanical ventilator)
    Ptrach[:, 1] .= tracheal_pressure(t[1])                               

    # File to monitor the simulation 
    output_mech = open("output_mech.txt", "w")

    # Time loop
    for i = 2:lastindex(t)

        global Peep_tr
        global pos
        global Peep
        global Pplat
        global incr_Peep_trial

        alpha = 1
        println(output_mech, "*******time step number : ", i)
   
        # Change of position
        if t[i]>flip_time && flipped == false && prone_positioning == true
            flipped = true
            pos, SIP, Ppl = ReversePosition(pos, SIP, Ppl, ball_coords, x_eso)
        end

        # Peep trial
        if Peep_tr==true && t[i]>time_Peep_trial[Peep_step+1] 
            Peep_step += 1
            Peep, Pplat = PeepTrial(Peep, Pplat, incr_Peep_trial)
            if Peep_step == lastindex(time_Peep_trial)
                Peep_tr = false
            end
        end

        Ptrach[:, i] .= tracheal_pressure(t[i])
        rhs[:, i] = RHS(Mat_R, Ptrach[:, i], Ptrach[:, i-1], Ppl, SIP, V_tilde[:, i-1])

        #Newton iteration variables 
        k = 0
        Viter = V_tilde[:, i-1]
        Res = Viter
        V_incr = Viter

        while (norm(Res, 2) > tol || norm(V_incr, 2) > tol|| k < 2)
            println(output_mech, "--------------iteration number : ", k)
            M = System_Mat(Viter, Mat_R)
            try
                V_incr = solve_iter(Viter, M, rhs[:, i])
            catch DomainError
                println(output_mech, "Error in Newton iteration solving (Automatic Differentation) - Compartment : ", j,
                           " Time step : ", i)
            end

            Viter = Viter + alpha * V_incr
            Res = Residual(Viter, M, rhs[:, i])
            println(output_mech, "Residual norm : ", norm(Res, 2))
            println(output_mech, "Increment norm : ", norm(V_incr, 2))
            k = k + 1
            if (k == max_iter || isequal(norm(Res, 2), NaN))
                alpha -= 0.1
                k = 0
                println(output_mech, "      °°°°°°°°°New increment step coefficient : ", alpha)
                if (alpha < 0.1)
                    println("*******time step number : ", i)  
                    close(output_mech)
                    throw(DomainError(k, "Maximum newton iterations reached in the mechanical model"))
                end
                V_incr = V_tilde[:, i-1]
                Viter = V_tilde[:, i-1]
            end
        end
        println(output_mech, "°°°--> CONVERGED! <--°°°")
        V_tilde[:, i] = Viter
        V[:, i] = V_init + Viter


        for j = 1:2^N
            C = compliance(V[j, i], j)
            Comp[j, i] = C
        end

        # RU pressure computation 
        P_A[:, i] = V_tilde[:, i] ./ Comp[:, i] + P_A[:,1]

        # Atelectasis Model (Bates & Irving 2002)
        for j = 1:2^N
            incr = d_x(P_A[j, i], j)
            x[j, i] = x[j, i-1] + incr*delta_t
            if x[j, i] >= 1.0
                x[j, i] = 1.0
            elseif x[j, i] <= 0.0
                x[j, i] = 0.0
            end
            state[j, i] = is_open(state[j, i-1], x[j, i])
        end
        for j = 1:2^N
            if state[j, i] == false
                V_tilde[j, i] =0
                V[j, i] = V[j, i-1]
                Comp[j, i] = 0
            end
        end

    end

    close(output_mech)

end