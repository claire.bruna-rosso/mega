"""
function compliance(V,i)
Compliance computations
V : current compartment volume
i : index of the compartment
"""
function compliance(Vi,i)

    # Initialization
    C = 6.5e-4

    # Computation of the compliance according to various models (uncomment the chosen one)
    #C = C_1(Vi)
    C = C_2(Vi)
    #C = C_3(Vi)

    C = C*coeff[i]
    
    return C
end

"""
function elastance(V)
Elastance computation
V : current compartment volume
i : index of the compartment
"""
function elastance(Vi)

    C = 6.5e-4
    C = C_2(Vi)
    E = 1 / C

    return E
end

"""
function C_1(V)
Compliance computation according to the model used in Swan et al. (2012)
V : current compartment volume
"""
function C_1(Vi)

    V0 = V_FRC/2^N
    
    if Vi>0
        lambda = (Vi/V0)^(1/3)
    else
        lambda = 1e-6
    end
    lambda_star = 1.15
    gamma = 3/4*(3*a+b)*(lambda_star^2-1)^2
    C_star = ksi*exp(gamma)/(6*V0)*(3*(3*a+b)^2*
                (lambda_star^2-1)^2/lambda_star^2+(3*a+b)*
                (lambda_star^2+1)/lambda_star^4)
    C_star = 1/C_star
    if lambda < lambda_star
        C = C_star/lambda_star*lambda
    else
        gamma = 3/4*(3*a+b)*(lambda^2-1)^2
        C = ksi*exp(gamma)/(6*V0)*(3*(3*a+b)^2*(lambda^2-1)^2/lambda^2+(3*a+b)*(lambda^2+1)/lambda^4)
        C = 1/C
        
    end
    C = C/0.010197 #conversion to cmH2O.L-1
    return C
end

"""
function C_2(V)
Compliance computation according to the model from Birzle et al. (2019)
V : current compartment volume
"""
function C_2(Vi)

    V0 = coeff_V0*V_FRC / 2^N

    # Jacobian, assuming a homogenous isotropic deformation (F = diag(lambda))
    if Vi>V0
        J = Vi/V0
    else
        J = 1
    end
    
    # Expression of the elastance, assuming a strain energy function as in Birzle et al. (2019)
    E = (8*J*c*(J^2)^(beta + 2/3) - (8*J^(7/3)*c)/(J^2)^(1/3) - 8*J^(1/3)*c*(J^2)^(2/3) + 12*J*c*(J^2)^(beta + 2/3)*(beta + 1) + 
        (8*J^(1/3)*c3*d3*((J^2)^(1/3) - 1)^(d3 - 1)*(J^2)^(beta + 1))/3 + 4*J^(7/3)*c3*d3*((J^2)^(1/3) - 1)^(d3 - 1)*(J^2)^beta*(beta + 1) + 
        (4*J^(7/3)*c3*d3*((J^2)^(1/3) - 1)^(d3 - 2)*(J^2)^(beta + 1/3)*(d3 - 1))/3)/(3*J^(1/3)*(J^2)^(beta + 5/3)) - (6*c*(J^2)^(beta + 5/3) - 
        6*J^(4/3)*c*(J^2)^(2/3) + 2*J^(4/3)*c3*d3*((J^2)^(1/3) - 1)^(d3 - 1)*(J^2)^(beta + 1))/(9*J^(4/3)*(J^2)^(beta + 5/3)) - 
        (2*J^(2/3)*(beta + 5/3)*(6*c*(J^2)^(beta + 5/3) - 6*J^(4/3)*c*(J^2)^(2/3) + 2*J^(4/3)*c3*d3*((J^2)^(1/3) - 1)^(d3 - 1)*(J^2)^(beta + 1)))/(3*(J^2)^(beta + 8/3))


    E /= V0

    C = 1 / E # compliance
    C *= 0.5#calibration coefficient
    C = C / 0.010197 #conversion to cmH2O.L-1

    return C
end

"""
function C_3(V)
Compliance computation according to the model used in Martin & Maury (2013)
V : current compartment volume
"""
function C_3(Vi)

    V_TLC = V_TLC/2^N#total lung capacity volume
    V_FRC = V_FRC/2^N # functional residual capacity volume
    V_RV = V_RV/2^N # residual volume
    a = 1/(V_TLC-V_FRC)^2
    b= 1/(V_FRC-V_RV)^2
    
    lambda = 2^N*E0/(a+b)
    
    C = (1/lambda)*((V_TLC-Vi)^2*(Vi-V_RV)^2)/((Vi-V_RV)^2+(V_TLC-Vi)^2)
    return C

end