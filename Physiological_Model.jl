# Include the Heaviside function
include("Heaviside.jl")

# Include the Hill's function (Saturation of the oxygen) and its derivative (Martin&Maury, 2013)
include("Hill_Saturation_function.jl")
include("NewtonIter_Phys.jl")
using DifferentialEquations

function PhysiologicalModel(P_cap, c_A, PO2_alv)

   # Initial conditions 
   P_cap[:, 1] = Pv * ones(2^N)
   c_A[:, 1] = 0.11 *ones(2^N) 

   # Number of hearbeats over the entire simulation 
   N_s = Int(floor(t[end]/tau_b))

   # Definition of the vector D, diffusion coefficient of the alveolar-capillary membrane
   D = Dm.*coeff

   # Duration of a heart period
   HeartPeriod = 0
   HeartPeriod = argmin(broadcast(abs,(t-tau_b*ones(length(t)))))

   # Conversion of P_A from cmH20 to mmHg
   P_A_Hg = P_A*0.74
  
   output_phys = open("output_phys.txt", "w")
   println(output_phys, "*******Heart period : ", HeartPeriod)

   # Auxiliary variables 
   kappa = D/(sigma*Vc)/22.4 # division by O2 molar volume to have kappa in s-1 

   # Time loop
   for i = 2:lastindex(t)

      for j = 1:2^N

         if state[j,i] #opened respiratory units
            
            # Variables to simplify equation expressions
            delta = MG/MO * (P_A_Hg[j,i]+Patm-P_vapor)
            
            # Resolution using DifferentialEquations
            dV = (V[j,i]-V[j,i-1])/delta_t

            p = V[j,i],dV,c0,D[j],delta,kappa[j],sigma,C 
            cP0=[c_A[j,i-1], P_cap[j,i-1]]
            tspan = (t[i-1],t[i])
            prob = ODEProblem(PhysioSystem!,cP0,tspan,p)
            sol = solve(prob)

            P_cap[j,i] = sol.u[end][2]

            c_A[j,i] = sol.u[end][1]
         
            PO2_alv[j,i] = delta*c_A[j,i] 

         else
            # Closed respiratory unit
            P_cap[j, i] = Pv
            c_A[j,i] = c_A[j,i-1]
            PO2_alv[j,i] = Pv
         end
      end 

      if(mod((i - 1),HeartPeriod) == 0) # Test if we are at a time of a heartbeat
   
            # Update the Initial value of the pressure in the capillaries 
   
            P_cap[:,i] = P_cap[:,1]
   
      end  
   end

   close(output_phys)

end

function PhysioSystem!(dcP,cP,p,t)
   V,dV,c0,Dm,delta,kappa,sigma,C = p
   dcP[1]=1/V*(dV*(c0-cP[1])*Heaviside(dV)-Dm*(delta*cP[1]-cP[2]))
   dcP[2]= kappa/(1+4*C/sigma*Saturation_prime(cP[2]))*(delta*cP[1]-cP[2])
end

