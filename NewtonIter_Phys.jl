# Physiological model Newton iteration related functions

"""
Function computing the residual in the Newton iteration to compute the capillary O2 partial pressure
"""
function Res_phys(P_iter,P_old,kappa,lambda,Dm,A,RHS1)
    
    Res = (1 + delta_t*kappa + 4*C/sigma*Saturation_prime(P_iter) + delta_t*delta_t*kappa*lambda*Dm/A)*P_iter-4*P_old*C/sigma*Saturation_prime(P_iter)-P_old-delta_t*kappa*lambda/A*RHS1

    return Res
    
end

"""
Newton iteration residual derivative
"""
function Res_phys_prime(P_iter,P_old,kappa,lambda,Dm,A,RHS1)

   
    Res_prime = ForwardDiff.derivative(P_iter->Res_phys(P_iter,P_old,kappa,lambda,Dm,A,RHS1),P_iter)
    
    return Res_prime
    
end


"""
Iteration resolution (increment computation)
"""
function solve_iter_phys(P_iter,P_old,kappa,lambda,Dm,A,RHS1)
    
    Res = Res_phys(P_iter,P_old,kappa,lambda,Dm,A,RHS1)
    Res_prime = Res_phys_prime(P_iter,P_old,kappa,lambda,Dm,A,RHS1)
    P_incr = -Res_prime\Res

    return P_incr
end
